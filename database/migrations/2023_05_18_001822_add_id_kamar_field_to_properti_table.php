<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('properti', function (Blueprint $table) {
            $table->unsignedBigInteger('id_kamar')->nullabe();
            $table->foreign('id_kamar')->references('id')->on('kamar')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('properti', function (Blueprint $table) {
            $table->dropForeign('properti_id_kamar_foreign');
            $table->dropColumn('id_kamar');
        });
    }
};
