<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('barang', function (Blueprint $table) {
            $table->unsignedBigInteger('id_gudang')->nullable();
            $table->foreign('id_gudang')->references('id')->on('gudang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('barang', function (Blueprint $table) {
            // format nama foreign = nama tabel nama kolom foreign _ tulisan foreign
            $table->dropForeign('barang_id_gudang_foreign');
            $table->dropColumn('id_gudang');
        });
    }
};
