<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>list customer</title>
</head>
<body>
    <h1>list customer</h1>
    <a href="{{ route ('customer.form-input') }}">Tambah Customer</a>
    <table border="1" width="100%">
        <tr>
            <th>No.</th>
            <th>Nama Customer</th>
            <th>Nomor Telepon</th>
            <th>Alamat</th>
        </tr>
        @php $no=1; @endphp
        @foreach($listDataCustomer as $row)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $row->nama_customer }}</td>
            <td>{{ $row->nomor_telepon }}</td>
            <td>{{ $row->alamat_customer }}</td>
        </tr>
        @endforeach
     
    </table>
</body>
</html>