<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>halaman form Input customer</title>
</head>
<body>
 
    <h1>Form Input Customer</h1>
    @if (session('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session('error') }}
    </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('success') }}
        </div>
    @endif
    <form action="{{ route('customer.simpan-data') }}" method="POST">
        @csrf
        <table width="50%" border="1" bgcolor="#DCDCDC">
            <tr bgcolor="#800080" align="center">
                <td colspan="2">
                    <font color="yellow">Form Input Customer</font>
                </td>
            </tr>
            <tr>
                <td>Nama Customer:</td>
                <td>
                    <input type="text" class="form-control" name="nama_customer" id="nama_customer"
                        value="{{ old('nama_customer') }}" required />
                </td>
            </tr>
            <tr>
                <td>Nomor Telepon:</td>
                <td>
                    <input type="text" class="form-control" name="nomor_telepon" id="nomor_telepon"
                        value="{{ old('nomor_telepon') }}" required />
                </td>
            </tr>
            <tr>
                <td>Alamat Customer:</td>
                <td>
                    <textarea class="form-control" name="alamat_customer" id="alamat_customer" cols="30" rows="10">{{ old('alamat_customer') }}</textarea>
                </td>
            </tr>
            <tr align="center">
                <td colspan="2">
                    <input type="submit" value="Simpan">
                    <input type="reset" name="" id="" value="Hapus">
                    <a href="{{ route('customer.list') }}" class="btn btn-danger">Kembali</a>
                </td>
            </tr>
        </table>
    </form>

</body>
</html>