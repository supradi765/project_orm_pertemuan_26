<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>form edit form</title>
</head>
<body>
    <h1>Form edit data</h1>
    @if (session('error'))
        {{ session('error') }}
    @endif
    @if(session('success'))
    {{ session('success') }}
@endif
    <form action="{{ route('kamar.edit-data', $getDataById->id)  }}" method="POST">
      @csrf
        <label for="">Nama Kamar</label>
        <input type="text" name="nama_kamar" id="nama_kamar" value="{{ $getDataById->nama_kamar }}"> <br>
        <label for="">Kapasitas Kamar</label>
        <input type="text" name="kapasitas" id="kapasitas" value="{{ $getDataById->kapasitas }}"> <br>
        <input type="submit" value="Simpan"/>
        <a href="{{ route('kamar.list') }}">kembali</a>
    </form>
</body>
</html>
