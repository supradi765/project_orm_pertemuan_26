<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Properti extends Model
{
    use HasFactory;

    protected $table = "properti";

    public $timestaps = true;

    protected $fillable = [
        'nama_properti',
        'jumlah_properti',
        'id_kamar'
    ];
}
